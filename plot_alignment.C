#include <iostream>
#include <string>
#include <stdio.h>

#include <TCanvas.h>
#include <TGraphAsymmErrors.h>
#include <TPaveText.h>
#include <TH2.h>
#include <TMultiGraph.h>
#include <TLegend.h>
#include <TStyle.h>
#include <TFile.h>


void plot_alignment() {
    bool debug = false;

    // paths and files
    auto input_path_pos = "align_pos.csv";
    auto input_file_pos = fopen(input_path_pos, "r");
    auto input_path_ang = "align_ang.csv";
    auto input_file_ang = fopen(input_path_ang, "r");
    auto output_file_path = "align.root";
    auto output_file = new TFile(output_file_path, "recreate");

    // canvases
    auto canvas_pos = new TCanvas("canvas1", "canvas1", 1600, 1200);
    auto canvas_ang = new TCanvas("canvas2", "canvas2", 1600, 1200);

    // initialize histograms
    std::vector<std::string> dets = {"timing", "dut", "telescope0", "telescope1", "telescope2", "telescope3", "telescope4", "telescope5"};
    std::map<std::string, TGraph*> gr1D;
    std::vector<std::string> axes = {"x", "y", "z"};

    for (const auto& det : dets) {
        for (const auto& a : axes) {
            if (debug) std::cout << "[DEBUG] " << "Initializing graph " << det << "_pos_" << a << std::endl;
            gr1D[det + "_pos_" + a] = new TGraph();
            gr1D[det + "_pos_" + a]->SetNameTitle(TString(det + "_pos_" + a), TString("Alignment position accuracy (" + a + ") for " + det + ";Threshold [fC];#Delta(true-align) [#mum]"));

            if (debug) std::cout << "[DEBUG] " << "Initializing graph " << det << "_ang_" << a << std::endl;
            gr1D[det + "_ang_" + a] = new TGraph();
            gr1D[det + "_ang_" + a]->SetNameTitle(TString(det + "_ang_" + a), TString("Alignment rotation accuracy (" + a + ") for " + det + ";Threshold [fC];#Delta(true-align) [deg]"));
        }
    }

    // // add grid
    canvas_pos->SetGrid(1,1);
    canvas_ang->SetGrid(1,1);

    // variables to hold data from csv files
    char line[80];
    float thr{};
    char det[20];
    char last_det[20];
    float true_pos_X{}, true_pos_Y{}, true_pos_Z{}, fin_pos_X{}, fin_pos_Y{}, fin_pos_Z{};
    float true_ang_X{}, true_ang_Y{}, true_ang_Z{}, fin_ang_X{}, fin_ang_Y{}, fin_ang_Z{};

    // read positions from csv file
    while (fgets(line, 80, input_file_pos)) {
        if (line[0] == '#')
            continue;

        // parse line
        sscanf(line, "%f %s %f %f %f %f %f %f", &thr, det, &true_pos_X, &true_pos_Y, &true_pos_Z, &fin_pos_X, &fin_pos_Y, &fin_pos_Z);

        // convert from e to fC
        thr /= 6241.509;

        // add points to graphs
        gr1D[std::string(det) + "_pos_x"]->AddPoint(thr, fin_pos_X - true_pos_X);
        gr1D[std::string(det) + "_pos_y"]->AddPoint(thr, fin_pos_Y - true_pos_Y);
        gr1D[std::string(det) + "_pos_z"]->AddPoint(thr, fin_pos_Z - true_pos_Z);
    }
    // close csv file
    fclose(input_file_pos);

    // read orientations from csv file
    while (fgets(line, 80, input_file_ang)) {
        if (line[0] == '#')
            continue;

        // parse line
        sscanf(line, "%f %s %f %f %f %f %f %f", &thr, det, &true_ang_X, &true_ang_Y, &true_ang_Z, &fin_ang_X, &fin_ang_Y, &fin_ang_Z);

        std::string str_det = det;
        std::string str_last_det = last_det;
        if (str_det == str_last_det) {
            continue;
        }

        // convert from e to fC
        thr /= 6241.509;

        // add points to graphs
        gr1D[std::string(det) + "_ang_x"]->AddPoint(thr, fin_ang_X - true_ang_X);
        gr1D[std::string(det) + "_ang_y"]->AddPoint(thr, fin_ang_Y - true_ang_Y);
        gr1D[std::string(det) + "_ang_z"]->AddPoint(thr, fin_ang_Z - true_ang_Z);
        // if (debug) std::cout << "Adding angle points for det " << det << " (last det " << last_det << ") thr " << thr << ": (" << fin_ang_X - true_ang_X << ", " << fin_ang_Y - true_ang_Y << ", " << fin_ang_Z - true_ang_Z << ")" << std::endl;
        strcpy(last_det, det);
    }
    // close csv file
    fclose(input_file_ang);

    // draw positions
    for (const auto& det : dets) {
        if (debug) std::cout << "[DEBUG] Writing graphs for detector \'" << det << "\'" << std::endl;
        canvas_pos->cd();
        for (const auto& a : axes) {
            gr1D[det + "_pos_" + a]->Write();
        }

        canvas_ang->cd();
        for (const auto& a : axes) {
            gr1D[det + "_ang_" + a]->Write();
        }
    }

    // write the output file and close
    output_file->Write();
    output_file->Close();
}
