# Rerun simulations on demand until run is valid

GEOMS=( "R2_0deg" "R2_5deg" "R2_10deg" "R2_15deg" "R2_20deg" "R2_25deg" "R2_0deg_alt" )

for GEOM in ${GEOMS[@]}; do
    declare RUNS

    # Parse result file to find bad runs
    EFF_FILE_BACK="reco/DESY_DURANTA_R2/$GEOM/results/tmp_efficiency.csv"
    EFF_FILE="reco/DESY_DURANTA_R2/$GEOM/results/efficiency.csv"
    cp ${EFF_FILE} ${EFF_FILE_BACK}

    echo "Geometry $GEOM"

    while read LINE; do
        THR=$(echo $LINE | cut -d "," -f 1)
        EFF=$(echo $LINE | cut -d "," -f 2)

        # Skip if reading csv header
        [[ "${THR:0:1}" == '#' ]] && continue;

        # Check if lower than 1%
        if (( $(echo "$EFF < 1.0" | bc -l) )); then
            # echo "  - threshold $THR (eff=$EFF)"
            RUNS+=( $THR )
        fi
    done < ${EFF_FILE}

    # echo "Rerunning bad runs"

    # Rerun bad runs
    for R in ${RUNS[@]}; do

        # Set threshold in run script
        sed -i "s|^THR_LOW=[0-9]*|THR_LOW=$R|" run.sh
        sed -i "s|^THR_UP=[0-9]*|THR_UP=$R|" run.sh

        # Rerun until good run
        while true; do
            echo -n "  - threshold $R: "

            bash run.sh $GEOM > /dev/null

            # wait

            # Parse efficiency
            EFF=$(grep $R $EFF_FILE | cut -d "," -f 2)
            echo -n "eff=$EFF"

            # Check if lower than 1%
            if (( $(echo "$EFF > 1.0" | bc -l) )); then
                echo " ... good run"
                break
            else
                echo " ... bad run"
            fi
        done
    done

    unset RUNS
done
