#!/bin/bash

# Print usage/help statements
# No params
print_help() {
    # echo "Usage   : bash run.sh [GEOMETRY] [ORIENTATION] [angle_alignment=false] [THR_LOW=0] [THR_UP=45000] [THR_STEP=5000] [alignment_thr=10000] [EVENTS=1000] [do_misalignment=false]"
    # echo "Full    : bash run.sh \"itkSS-5degY\" \"0 5deg 0\" \"xyz\" 0 45000 100 10000 40000"
    # echo "Minimal : bash run.sh \"itkSS-5degY\" \"0 5deg 0\" "

    echo "Usage   : bash run.sh [GEOMETRY]"
}

get_workers() {
    echo $(($(ps --no-headers -o pid --ppid=${MAIN_PID} | wc -w)-1))
}

mem_proc() {
    ps --no-headers --ppid=${MAIN_PID} -o rss | awk '{rss += $1/1024} END {print rss}'
}

mem_used() {
    free -m | awk 'FNR == 2 {print $3}'
}

mem_avail() {
    free -m | awk 'FNR == 2 {print $7}'
}

mem_avail_per() {
    free -m | awk 'FNR == 2 {print $7/$2*100}'
}

time_() {
    echo "[$(date +%H:%M:%S)]"
}

ready() {
    # If run is prealignment, always valid
    if [[ $2 == "p" ]]; then
        echo 1
    else
        # See if earlier (pre-)alignment stages are done
        CHK_STAGES=( p $(seq 1 $((${STAGE}-1))) )

        READY=1
        for CHK in ${CHK_STAGES[@]}; do
            if [[  ${RUNS["${1}_${CHK}"]} -eq 0 ]]; then
                # echo "$N not done"
                READY=0
                break
            fi
        done

        echo $READY
    fi
}

waiting() {
    PRINT_NEWLINE=0
    # Do not submit runs if available memory below this
    AVAIL_MEM_LIM=25

    WAIT_FOR=1

    ACT_THREADS=$(get_workers)
    MEM_LIM=$(echo "$(mem_avail_per) < ${AVAIL_MEM_LIM}" | bc -l)

    while [[ ${ACT_THREADS} -ge $WORKERS ]] || (( ${MEM_LIM} )); do

        [[ ${ACT_THREADS} -ge $WORKERS ]] && W_COL="\e[31m" || W_COL="\e[33m"
        (( ${MEM_LIM} )) && M_COL="\e[31m" || M_COL="\e[33m"

        # echo -en "\r$(time_) Waiting [WOR_OCC = ${W_COL}${ACT_THREADS}/$WORKERS\e[0m, MEM_USED = \e[33m$(mem_used) MB\e[0m, MEM_PROC = \e[33m$(mem_proc) MB\e[0m, MEM_AVAIL = ${M_COL}$(mem_avail) MB ($(mem_avail_per) %)\e[0m]    "
        sleep $WAIT_FOR

        ACT_THREADS=$(get_workers)
        MEM_LIM=$(echo "$(mem_avail_per) < ${AVAIL_MEM_LIM}" | bc -l)

        PRINT_NEWLINE=1
    done

    # [[ $PRINT_NEWLINE -eq 1 ]] && echo
}

check_finished() {
    for RUN in ${!PID_MAP[@]}; do
        # echo -n  "Checking if process ${PID_MAP[$RUN]} (run $RUN) is still running: "
        if [[ $(ps --no-headers -p ${PID_MAP[$RUN]} | wc -l) -eq 0 ]]; then
            RUNS[$RUN]=1
            unset PID_MAP[$RUN]
        fi
        # echo
    done
}

runs_remaining() {
    REM=0
    for R in ${!RUNS[@]}; do
        [[ ${RUNS[$R]} -ne 1 ]] && REM=$((REM+1))
    done
    echo $REM
}


run_allpix() {
    echo -e "\n\e[34;1m[SIM]\e[0m Running Allpix-Squared\e[0m simulations with thresholds \e[34;1m${THR_LOW}e\e[0m to \e[34;1m${THR_UP}e\e[0m with step of \e[34;1m${THR_STEP}e\e[0m (Run name: \e[34;1m${GEOMETRY}\e[0m)"

    # Run simulations
    for THR in $(seq $THR_LOW $THR_STEP $THR_UP); do
        echo -en "\t[$((($THR-${THR_LOW})/${THR_STEP}+1))/$(((${THR_UP}-${THR_LOW})/${THR_STEP}+1))]  \tThreshold ${THR}e    \t"

        # See if this run already has simulation done
        if [ -f ${SIM_DIR}/output/thr${THR}.root ] && [ $OVERRIDE -ne 1 ]; then
            echo -e " \e[33m[done, skipping]\e[0m"
            continue
        fi

        # Run Allpix
        ${ALLPIX_EXEC}\
            -c ${CONFIG_DIR}/allpix_main.conf\
            -o number_of_events=${EVENTS}\
            -o output_directory=${SIM_DIR}/output\
            -o detectors_file="${CONFIG_DIR}/geometries/${GEOMETRY}.conf"\
            -o root_file=${SIM_DIR}/output/thr${THR}.root\
            -o log_file=${APSQ_LOG_DIR}/thr${THR}.log\
            -o DefaultDigitizer:dut.threshold=${THR}e\
            -o CorryvreckanWriter.file_name=corry_thr${THR}.root\
            -o CorryvreckanWriter.geometry_file=geom_thr${THR}.conf 1>/dev/null 2>/dev/null

        echo -e " \e[34;1m[done]\e[0m"
    done
}


# Runs alignment stage of the reconstruction
# Param 1 (bool): Telescope alignment
# Param 2 (bool): DUT alignment
run_align() {
    echo -ne "\e[34;1m[ALIGN]\e[0m Running alignment\e[0m ("
    if [ $1 -eq 1 ]; then echo -en "\e[32mTele \e[0m"; else echo -en "\e[31mTele \e[0m"; fi
    if [ $2 -eq 1 ]; then echo -e "\e[32mDUT)\e[0m"; else echo -e "\e[31mDUT)\e[0m"; fi

    # If telescope alignment already done, skip it
    if [[ $1 -eq 1 ]] && [ -f ${RECO_DIR}/geometry/telescope.conf ]; then
        echo -e "        \e[33mTelescope alignment already done, skipping\e[0m"
        skip_telescope=1
    fi

    # Telescope alignment
    if [[ $1 -eq 1 && skip_telescope -ne 1 ]]; then
        echo -ne "        Running telescope alignment on threshold ${THR_ALIGN}: "

        echo -ne "\e[34;1m[Align 1]  "
        ${CORRY_EXEC} \
            -c ${CONFIG_DIR}/corry/align_tele1.conf \
            -o detectors_file=${SIM_DIR}/output/CorryvreckanWriter/geom_thr${THR_ALIGN}.conf \
            -o detectors_file_updated=${RECO_DIR}/geometry/telescope.conf \
            -o output_directory=${RECO_DIR}/output \
            -o histogram_file=align_tele1.root \
            -o FileReader.file_name=${SIM_DIR}/output/CorryvreckanWriter/corry_thr${THR_ALIGN}.root \
            -o log_file=${CORRY_LOG_DIR}/align_tele1.log > /dev/null

        echo -ne "\e[34;1m[Align 2]  "
        ${CORRY_EXEC} \
            -c ${CONFIG_DIR}/corry/align_tele2.conf \
            -o detectors_file=${RECO_DIR}/geometry/telescope.conf \
            -o detectors_file_updated=${RECO_DIR}/geometry/telescope.conf \
            -o output_directory=${RECO_DIR}/output \
            -o histogram_file=align_tele2.root \
            -o FileReader.file_name=${SIM_DIR}/output/CorryvreckanWriter/corry_thr${THR_ALIGN}.root \
            -o log_file=${CORRY_LOG_DIR}/align_tele2.log > /dev/null

        echo -ne "\e[34;1m[Align 3]  "
        ${CORRY_EXEC} \
            -c ${CONFIG_DIR}/corry/align_tele3.conf \
            -o detectors_file=${RECO_DIR}/geometry/telescope.conf \
            -o detectors_file_updated=${RECO_DIR}/geometry/telescope.conf \
            -o output_directory=${RECO_DIR}/output \
            -o histogram_file=align_tele3.root \
            -o FileReader.file_name=${SIM_DIR}/output/CorryvreckanWriter/corry_thr${THR_ALIGN}.root \
            -o log_file=${CORRY_LOG_DIR}/align_tele3.log > /dev/null

        echo -ne "\e[34;1m[Align 4]  "
        ${CORRY_EXEC} \
            -c ${CONFIG_DIR}/corry/align_tele4.conf \
            -o detectors_file=${RECO_DIR}/geometry/telescope.conf \
            -o detectors_file_updated=${RECO_DIR}/geometry/telescope.conf \
            -o output_directory=${RECO_DIR}/output \
            -o histogram_file=align_tele4.root \
            -o FileReader.file_name=${SIM_DIR}/output/CorryvreckanWriter/corry_thr${THR_ALIGN}.root \
            -o log_file=${CORRY_LOG_DIR}/align_tele4.log > /dev/null

        echo -ne "\e[34;1m[Align 5]  "
        ${CORRY_EXEC} \
            -c ${CONFIG_DIR}/corry/align_tele5.conf \
            -o detectors_file=${RECO_DIR}/geometry/telescope.conf \
            -o detectors_file_updated=${RECO_DIR}/geometry/telescope.conf \
            -o output_directory=${RECO_DIR}/output \
            -o histogram_file=align_tele5.root \
            -o FileReader.file_name=${SIM_DIR}/output/CorryvreckanWriter/corry_thr${THR_ALIGN}.root \
            -o log_file=${CORRY_LOG_DIR}/align_tele5.log > /dev/null

        echo -ne "\e[34;1m[Align 6]"
        ${CORRY_EXEC} \
            -c ${CONFIG_DIR}/corry/align_tele6.conf \
            -o detectors_file=${RECO_DIR}/geometry/telescope.conf \
            -o detectors_file_updated=${RECO_DIR}/geometry/telescope.conf \
            -o output_directory=${RECO_DIR}/output \
            -o histogram_file=align_tele6.root \
            -o FileReader.file_name=${SIM_DIR}/output/CorryvreckanWriter/corry_thr${THR_ALIGN}.root \
            -o log_file=${CORRY_LOG_DIR}/align_tele6.log > /dev/null

        echo -e "\e[0m"
    fi

    # DUT alignment
    # Array for runs and process IDs
    declare -A RUNS
    declare -A PID_MAP

    # Generate array of runs
    for R in $(seq $THR_LOW $THR_STEP $THR_UP); do
        RUNS[${R}_p]=1
        for N in $(seq 1 4); do
            RUNS[${R}_${N}]=0
        done
    done

    # Process alignment runs
    if [ $2 -eq 1 ]; then
        while [[ $(runs_remaining) -ne 0 ]]; do
            # Check if any running process has finished
            check_finished

            for R in ${!RUNS[@]}; do
                # Skip if already finished or running
                if [[ ${RUNS[$R]} -eq 1 ]] || [[ -n "${PID_MAP[$R]}" ]] ; then
                    continue
                fi

                # Wait if workers are occupied or memory usage is high
                waiting

                # Check if run can be submitted
                THR=$(echo "$R" | cut -f 1 -d "_")
                STAGE=$(echo "$R" | cut -f 2 -d "_")
                if [[ $(ready $THR $STAGE) -ne 1 ]]; then
                    continue
                fi

                # Choose correct geometry file based on alignment stage
                [[ $STAGE -eq 1 ]] && DET_FILE="${RECO_DIR}/geometry/telescope.conf" || DET_FILE="${RECO_DIR}/geometry/thr${THR}.conf"

                # Submit run
                echo -en "$(time_) Submitting run \e[34;1m$THR [$STAGE]\e[0m"
                ${CORRY_EXEC} \
                -c ${CONFIG_DIR}/corry/align_dut${STAGE}.conf \
                -o detectors_file=${DET_FILE} \
                -o detectors_file_updated=${RECO_DIR}/geometry/thr${THR}.conf \
                -o output_directory=${RECO_DIR}/output  \
                -o histogram_file=thr${THR}_align${STAGE}.root \
                -o FileReader.file_name=${SIM_DIR}/output/CorryvreckanWriter/corry_thr${THR}.root \
                -o log_file=${CORRY_LOG_DIR}/thr${THR}_align${STAGE}.log > /dev/null &

                # Enter PID of the started run into a map
                PID=$!
                PID_MAP[$R]=$PID

                echo -e ", remaining \e[34;1m$(($(runs_remaining)-${#PID_MAP[@]}))\e[0m "

                # Check running processes
                check_finished

                sleep 1
            done

            # If all runs have been submitted, end the loop
            [[ $(($(runs_remaining)-${#PID_MAP[@]})) -eq 0 ]] && break

            sleep 1
        done

        echo "$(time_) Waiting for final runs to finish"
        wait
        echo "$(time_) Done"
    fi
}

# Runs alignment stage of the reconstruction
# No params
run_analysis() {
    echo -e "\e[34;1m[ANALYSIS]\e[0m Running analysis\e[0m"

    RUNS=$(seq $THR_LOW $THR_STEP $THR_UP)
    REM_RUNS=(${RUNS[@]})

    for THR in ${RUNS[@]}; do
        # Wait if too many threads are running
        waiting

        echo -ne "$(time_) Submitting threshold \e[34;1m${THR}\e[0m run"

        ${CORRY_EXEC} \
            -c ${CONFIG_DIR}/corry/analyze.conf \
            -o detectors_file=${RECO_DIR}/geometry/thr${THR}.conf \
            -o output_directory=${RECO_DIR}/output  \
            -o histogram_file=thr${THR}_analysis.root \
            -o FileReader.file_name=${SIM_DIR}/output/CorryvreckanWriter/corry_thr${THR}.root \
            -o log_file=${CORRY_LOG_DIR}/thr${THR}_analysis.log > /dev/null &

        # Remove submitted run from runlist
        REM_RUNS=( ${REM_RUNS[@]:1} )

        echo -e " (runs remaining = \e[34;1m${#REM_RUNS[@]}\e[0m)"

        sleep 1

        # eff=`cat ${CORRY_LOG_DIR}/thr${THR}_analysis.log | grep 'Total efficiency of detector' | grep -oE :[[:space:]][0-9]*.*%`
        # total=`echo ${eff%(*} | grep -oE [0-9].*`
        # echo -e " efficiency = $total%"
    done

    echo "$(time_) Waiting for final runs to finish"
    wait
    echo "$(time_) Done"
}

extract_efficiencies() {
    echo -e "\e[34;1mExtracting efficiencies from log files.\e[0m"

    rm -f ${RECO_DIR}/results/efficiency.csv
    touch ${RECO_DIR}/results/efficiency.csv

    # Write CSV header
    echo "# thr, efficiency, error_up, error_down"  >> ${RECO_DIR}/results/efficiency.csv

    # Iterate over thresholds
    for THR in $(seq ${THR_LOW} ${THR_STEP} ${THR_UP}); do
            eff=`cat ${CORRY_LOG_DIR}/thr${THR}_analysis.log | grep 'Total efficiency of detector' | grep -oE :[[:space:]][0-9]*.*%`
            if [ -z "$eff" ]; then
                echo "        Empty eff string at thr $THR, filling zero efficiency."
                echo "$THR,0,0,0"  >> ${RECO_DIR}/results/efficiency.csv
                continue
            fi
            total=`echo ${eff%(*} | grep -oE [0-9].*`
            er_up=`echo ${eff} | grep -oE "\(.*[[:space:]]" | grep -oE "[0-9].*"`
            er_up=${er_up:0:-1}
            er_low=`echo ${eff##*-} | grep -oE ".*)"`
            er_low=${er_low:0:-1}

            # Write efficiency and errors into the csv file
            echo "$THR,$total,$er_up,$er_low"  >> ${RECO_DIR}/results/efficiency.csv
    done
}

plot_efficiency() {
    echo -e "\e[34;1mPlotting efficiencies\e[0m (plot name \e[34;1m${GEOMETRY}\e[0m)."

    cp ${RECO_DIR}/results/efficiency.csv eff.csv

    # Run plot macro
    root -b -q -l plot_efficiencies.C 1>/dev/null 2>/dev/null

    # Delete temporary csv
    rm eff.csv

    # Rename plot to run name
    mv plot.pdf ${RECO_DIR}/results/efficiency.pdf
    # mv eff.root ${RECO_DIR}/results/efficiency.root
}

extract_cluster_sizes () {
    echo -e "\e[34;1mExtracting mean cluster sizes from analysis output files:\e[0m"

    # Make new csv file
    rm -f ${RECO_DIR}/results/cluster.csv
    touch ${RECO_DIR}/results/cluster.csv

    # Write csv file header
    echo -e "# thr, mean_cluster_size, mean_error" >> ${RECO_DIR}/results/cluster.csv

    # Iterate over thresholds
    for THR in $(seq ${THR_LOW} ${THR_STEP} ${THR_UP}); do
        echo -en "\t[$((($THR-${THR_LOW})/${THR_STEP}+1))/$(((${THR_UP}-${THR_LOW})/${THR_STEP}+1))]  \tThreshold ${THR}e    \t"

        # Skip if output file doesn't exist
        if [ ! -f ${RECO_DIR}/output/thr${THR}_analysis.root ]; then
            continue
        fi

        # Process cluster size histogram
        output=$(root -l -b -q "calc_cluster_sizes.C(\"$CAMPAIGN\",\"$GEOMETRY\",$THR)")
        # Parse mean size and error
        mean=$(echo $output | grep -oE "Mean=.*," | grep -oE "[0-9.]*")
        error=$(echo $output | grep -oE "Error=.*\(" | grep -oE "[0-9.]*")
        echo " $mean +- $error"

        # Write to csv file
        if [[ "$mean" != "0" ]] && [[ ! -z "$mean" ]]; then
            echo "$THR,$mean,$error" >> ${RECO_DIR}/results/cluster.csv
        fi
    done
}

convert_position() {
    # echo "Converting $1"
    num=${1:0:-2}
    if [[ "$1" =~ .*"um".* ]]; then
        echo "$num"
        # return $((${1:0:-2}))
    else if [[ "$1" =~ .*"mm".* ]]; then
        echo "$(expr $num*1000 | bc)"
        # return $((${1:0:-2}))
        fi
    fi
}

extract_alignment() {
    echo -e "\e[34;1mStarting alignment performance analysis.\e[0m"

    # Make new csv file
    rm -f ${RECO_DIR}/results/alignment.csv
    touch ${RECO_DIR}/results/alignment.csv

    # Write csv file header
    echo -e "# thr, posX, posY, posZ, angX, angY, angZ" >> ${RECO_DIR}/results/alignment.csv

    # Iterate over thresholds
    for THR in $(seq ${THR_LOW} ${THR_STEP} ${THR_UP}); do
        echo -e "\t[$((($THR-${THR_LOW})/${THR_STEP}+1))/$(((${THR_UP}-${THR_LOW})/${THR_STEP}+1))]  \tThreshold ${THR}e    \t"

        # Skip if output file doesn't exist
        if [ ! -f ${RECO_DIR}/geometry/thr${THR}.conf ]; then
            continue
        fi

        # Line number for the proper detector config
        ln=$(cat ${RECO_DIR}/geometry/thr${THR}.conf | grep -n "\[dut\]" | awk '{split($0,a,":");print a[1]}')

        # Get true position
        pos=($(cat ${RECO_DIR}/geometry/thr${THR}.conf | head -n $((ln+15)) | tail -n 15 | awk '{ if($1 == "position") print $3,$4,$5}' | sed 's/,/ /g'))
        posX=$(convert_position ${pos[0]})
        posY=$(convert_position ${pos[1]})
        posZ=$(convert_position ${pos[2]})
        ang=($(cat ${RECO_DIR}/geometry/thr${THR}.conf  | head -n $((ln+10)) | tail -n 10 | awk '{ if($1 == "orientation") print $3,$4,$5}' | sed 's/,/ /g' | sed 's/deg//g'))
        angX=${ang[0]}
        angY=${ang[1]}
        angZ=${ang[2]}

        # echo -n "                        "
        # echo "THR=$THR, Position (x, y, z)=($posX, $posY, $posZ), Orientation (x, y, z)=($angX, $angY, $angZ)"

        echo "$THR,$posX,$posY,$posZ,$angX,$angY,$angZ" >> ${RECO_DIR}/results/alignment.csv
    done
}

extract_resolution () {
    echo -e "\e[34;1mExtracting resolution from analysis output files:\e[0m"

    # Make new csv file
    rm -f ${RECO_DIR}/results/resolution.csv
    touch ${RECO_DIR}/results/resolution.csv

    # Write csv file header
    echo -e "#thr,resolution,error" >> ${RECO_DIR}/results/resolution.csv

    # Iterate over thresholds
    for THR in $(seq ${THR_LOW} ${THR_STEP} ${THR_UP}); do
        echo -en "\t[$((($THR-${THR_LOW})/${THR_STEP}+1))/$(((${THR_UP}-${THR_LOW})/${THR_STEP}+1))]  \tThreshold ${THR}e    \t"

        # Skip if output file doesn't exist
        if [ ! -f ${RECO_DIR}/output/thr${THR}_analysis.root ]; then
            continue
        fi

        # Process cluster size histogram
        output=$(root -l -b -q "calc_resolution.C(\"$CAMPAIGN\",\"$GEOMETRY\",$THR)")
        # Parse mean size and error
        mean=$(echo $output | grep -oE "Resolution=.*," | grep -oE "[0-9.]*")
        error=$(echo $output | grep -oE "Error=.*\(" | grep -oE "[0-9.]*")
        echo " $mean +- $error"

        # Write to csv file
        if [[ "$mean" != "0" ]] && [[ ! -z "$mean" ]]; then
            echo "$THR,$mean,$error" >> ${RECO_DIR}/results/resolution.csv
        fi
    done
}

# ------------------------------
# -------- Main program --------
# ------------------------------

WORKERS=10
MAIN_PID=$$

# Corry executable
CORRY_EXEC=${CORRY_DIR}/corry
ALLPIX_EXEC=${APSQ_DIR}/allpix

# Check corry executable is sourced
if [ -z  "${CORRY_DIR}" ]; then
    echo -e "\e[31mEnvironmental variable CORRY_DIR empty, please source the setup.sh script.\e[0m"
    exit 1
fi

# Check corry executable is sourced
if [ -z  "${APSQ_DIR}" ]; then
    echo -e "\e[31mEnvironmental variable APSQ_DIR empty, please source the setup.sh script.\e[0m"
    exit 1
fi

# Help statement
if [[ "$1" == "-h" || "$1" == "--help" || -z "$1" ]]; then
    print_help
    exit 0
fi

# Run parameters
THR_LOW=0
THR_UP=45000
THR_STEP=1000
THR_ALIGN=10000
EVENTS=250000

DEBUG=0
if [[ $DEBUG -eq 1 ]]; then
    THR_LOW=1
    THR_UP=1
    THR_STEP=1
    THR_ALIGN=10000
    EVENTS=1
fi

if [ ! -z "$1" ]; then
    GEOMETRY="$1"
fi

OVERRIDE=0

CONFIG_DIR=${PROJECT_DIR}/configs/${CAMPAIGN}
SIM_DIR=${PROJECT_DIR}/sim/${CAMPAIGN}/${GEOMETRY}
RECO_DIR=${PROJECT_DIR}/reco/${CAMPAIGN}/${GEOMETRY}
APSQ_LOG_DIR=${PROJECT_DIR}/sim/${CAMPAIGN}/${GEOMETRY}/logs
CORRY_LOG_DIR=${PROJECT_DIR}/reco/${CAMPAIGN}/${GEOMETRY}/logs

# Debug printout
echo " --------------- DEBUG INFO ---------------"
echo " Corry exec     : ${CORRY_DIR}/corry"
echo " APSQ exec      : ${APSQ_DIR}/allpix"
echo " Project dir    : ${PROJECT_DIR}"
echo " Campaign       : ${CAMPAIGN}"
echo " Orientation    : ${GEOMETRY}"
if [[ $DEBUG -eq 1 ]]; then echo -e  " \e[33;1mDebug ON\e[0m"; fi
echo " Thr low        : ${THR_LOW}"
echo " Thr high       : ${THR_UP}"
echo " Thr step       : ${THR_STEP}"
echo " Thr align      : ${THR_ALIGN}"
echo " Events         : $EVENTS"
echo -e " ------------------------------------------"

# Make directories
mkdir -p ${CONFIG_DIR} \
         ${SIM_DIR}/output \
         ${RECO_DIR}/output \
         ${RECO_DIR}/geometry \
         ${RECO_DIR}/results \
         ${APSQ_LOG_DIR} \
         ${CORRY_LOG_DIR}

# Run Allpix-Squared simulations
run_allpix

# Run Corryvreckan reconstruction and analysis
run_align 1 1
run_analysis

# Analyse results
extract_efficiencies
plot_efficiency
extract_cluster_sizes
extract_alignment
extract_resolution
