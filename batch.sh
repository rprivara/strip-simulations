# bash run.sh [geometry]

############################
########## ITk R2 ##########
############################

CAMPAIGN="DESY_DURANTA_R2"

# ------------------------
# ----- SIM AND RECO -----
# ------------------------
bash run.sh "R2_0deg"
bash run.sh "R2_5deg"
bash run.sh "R2_10deg"
bash run.sh "R2_15deg"
bash run.sh "R2_20deg"
bash run.sh "R2_25deg"
bash run.sh "R2_0deg_alt"

# -----------------------
# --- PROCESS RESULTS ---
# -----------------------
TITLE="ATLAS ITk R2 (perpendicular)"
GEOM="R2_0deg"
root -l -b -q "gen_results.C(\"$CAMPAIGN\",\"$GEOM\",\"$TITLE\")"

TITLE="ATLAS ITk R2 (5deg)"
GEOM="R2_5deg"
root -l -b -q "gen_results.C(\"$CAMPAIGN\",\"$GEOM\",\"$TITLE\")"

TITLE="ATLAS ITk R2 (10deg)"
GEOM="R2_10deg"
root -l -b -q "gen_results.C(\"$CAMPAIGN\",\"$GEOM\",\"$TITLE\")"

TITLE="ATLAS ITk R2 (15deg)"
GEOM="R2_15deg"
root -l -b -q "gen_results.C(\"$CAMPAIGN\",\"$GEOM\",\"$TITLE\")"

TITLE="ATLAS ITk R2 (20deg)"
GEOM="R2_20deg"
root -l -b -q "gen_results.C(\"$CAMPAIGN\",\"$GEOM\",\"$TITLE\")"

TITLE="ATLAS ITk R2 (25deg)"
GEOM="R2_25deg"
root -l -b -q "gen_results.C(\"$CAMPAIGN\",\"$GEOM\",\"$TITLE\")"

TITLE="ATLAS ITk R2 (perpendicular)"
GEOM="R2_0deg_alt"
root -l -b -q "gen_results.C(\"$CAMPAIGN\",\"$GEOM\",\"$TITLE\")"
