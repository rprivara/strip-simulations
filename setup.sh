# Export project directory
export PROJECT_DIR=$(pwd)

# APSQ implementation
export CAMPAIGN="DESY_DURANTA_R2"
export CORRY_DIR=~/Git/ITk/corryvreckan/bin
export APSQ_DIR=~/Git/ITk/allpix-squared/bin

echo -e "Exporting campaign name: \e[32m${CAMPAIGN}\e[0m"
echo -e "Exporting corry path:    \e[32m${CORRY_DIR}\e[0m"

# Check if corry bin exists in the path
if [ -f ${CORRY_DIR}/corry ]; then
    echo -e "\e[32mCorry executable exists in the path.\e[0m"
else
    echo -e "\e[31mCorry executable doesn't exist in the path, please check the path again.\e[0m"
    return 1
fi

echo -e "Exporting APSQ path:    \e[32m${APSQ_DIR}\e[0m"

# Check if corry bin exists in the path
if [ -f ${APSQ_DIR}/allpix ]; then
    echo -e "\e[32mAPSQ executable exists in the path.\e[0m"
else
    echo -e "\e[31mAPSQ executable doesn't exist in the path, please check the path again.\e[0m"
    return 1
fi
