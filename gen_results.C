#include <iostream>
#include <string>
#include <algorithm>

#include <TCanvas.h>
#include <TGraphAsymmErrors.h>
#include <TGraphErrors.h>
#include <TPaveText.h>
#include <TF1.h>
#include <TFile.h>
#include <TGaxis.h>
#include <TAxis.h>
#include <TLegend.h>


int gen_results(std::string campaign = "", std::string geometry = "", std::string title="") {
    // Check passed arguments
    if (campaign == "" || geometry == "" || title == "") {
        std::cerr << "Invalid arguments passed" << std::endl;
        std::cout << "Usage: root gen_results.C([str: campaign],[str: geometry],[str: title])" << std::endl;
        return 1;
    }

    // Skip points with large errors or very low efficiency
    bool skip_errors = true;

    // Print histogram titles
    bool print_title = true;

    bool fit_efficiency = true;

    // Debug printouts
    bool debug = false;

    // -----------------------------
    // -------- LOAD INPUTS --------
    // -----------------------------

    // Input and output root files
    auto eff_file = fopen(("reco/" + campaign + "/" + geometry + "/results/efficiency.csv").c_str(), "r");
    auto clus_file = fopen(("reco/" + campaign + "/" + geometry + "/results/cluster.csv").c_str(), "r");
    auto align_file = fopen(("reco/" + campaign + "/" + geometry + "/results/alignment.csv").c_str(), "r");
    auto resol_file = fopen(("reco/" + campaign + "/" + geometry + "/results/resolution.csv").c_str(), "r");
    auto output_file = new TFile(("reco/" + campaign + "/" + geometry + "/results/results.root").c_str(), "recreate");

    auto use_title = print_title ? title : "";

    // Efficiency graph
    auto g_eff = new TGraphAsymmErrors();
    g_eff->SetNameTitle("Efficiency", (use_title + ";Threshold [fC];Efficiency").c_str());

    // Cluster size graph
    auto g_clus = new TGraphErrors();
    g_clus->SetNameTitle("ClusterSize",(use_title + ";Threshold [fC];Mean cluster size").c_str());

    // Cluster size graph
    auto g_res = new TGraphErrors();
    g_res->SetNameTitle("Resolution",(use_title + ";Threshold [fC];Spatial resolution [#murad]").c_str());

    // Alignment values
    std::vector<double> x, y, z, dx, dy, dz, th;
    // Alignment graphs
    auto g_posX = new TGraph();
    g_posX->SetNameTitle("PosX", (use_title + "; Threshold [fC];x_{DUT} [#mum]").c_str());
    auto g_posY = new TGraph();
    g_posY->SetNameTitle("PosY", (use_title + "; Threshold [fC];y_{DUT} [#mum]").c_str());
    auto g_posZ = new TGraph();
    g_posZ->SetNameTitle("PosZ", (use_title + "; Threshold [fC];z_{DUT} [#mum]").c_str());
    auto g_degX = new TGraph();
    g_degX->SetNameTitle("DegX", (use_title + "; Threshold [fC];#theta^{x}_{DUT} [deg]").c_str());
    auto g_degY = new TGraph();
    g_degY->SetNameTitle("DegY", (use_title + "; Threshold [fC];#theta^{y}_{DUT} [deg]").c_str());
    auto g_degZ = new TGraph();
    g_degZ->SetNameTitle("DegZ", (use_title + "; Threshold [fC];#theta^{z}_{DUT} [deg]").c_str());

    std::vector<unsigned int> skip_runs{};

    // Read efficiencies from the input CSV file and fill the graph
    std::cout << "Generating efficiency graph" << std::endl;
    char line[100];
    float thr{}, eff{}, err_low{}, err_high{};
    while (fgets(line, 100, eff_file)) {
        // Skip comment lines
        if (line[0] == '#')
            continue;

        // Read the next line
        sscanf(line, "%f,%f,%f,%f", &thr, &eff, &err_high, &err_low);

        if (debug) std::cout << "THR=" << thr << " (" <<  thr / 6241.509125 << " fC) : EFF=(" << eff << " +" << err_high << " -" << err_low << ")" << std::endl;

        if (skip_errors) {
            if (err_high > 5 || err_low > 5 || eff < 1) {
                skip_runs.push_back(thr);
                std::cout << "Run with threshold " << thr << " is to be skipped" << std::endl;
                continue;
            }
        }

        thr /= 6241.509125;

        // Fill graph and set errors
        g_eff->AddPoint(thr, eff/100);
        g_eff->SetPointError(g_eff->GetN()-1, 0, 0, err_low/100, err_high/100);
    }
    // Close the input efficiency file
    fclose(eff_file);

    // Read cluster sizes from the input CSV file and fill the graph
    std::cout << "Generating mean cluster size graph" << std::endl;
    float clus_mean{}, clus_error{};
    while (fgets(line, 100, clus_file)) {
        // Skip comment lines
        if (line[0] == '#')
            continue;


        // Read the next line
        sscanf(line, "%f,%f,%f", &thr, &clus_mean, &clus_error);


        if (std::find(skip_runs.begin(), skip_runs.end(), thr) != skip_runs.end()) {
                std::cout << "Skipping threshold " << thr << std::endl;
                continue;
        }

        thr /= 6241.509125;

        if (debug) std::cout << "THR=" << thr << " : CS=(" << clus_mean << " +- " << clus_error << ")" << std::endl;

        // Fill graph and set errors
        g_clus->AddPoint(thr, clus_mean);
        g_clus->SetPointError(g_clus->GetN()-1, 0, clus_error);
    }
    // Close the input efficiency file
    fclose(clus_file);

    // Read alignment from the input CSV file and fill the graph
    std::cout << "Generating alignment graphs" << std::endl;
    float posX, posY, posZ, degX, degY, degZ;
    while (fgets(line, 100, align_file)) {
        // Skip comment lines
        if (line[0] == '#')
            continue;

        // Read the next line
        sscanf(line, "%f,%f,%f,%f,%f,%f,%f", &thr, &posX, &posY, &posZ, &degX, &degY, &degZ);

        if (debug) std::cout << "THR=" << thr << " (" <<  thr / 6241.509125 << " fC) : pos=(" << posX << " " << posY << " " << posZ << "), deg=(" << degX << " " << degY << " " << degZ << ")" << std::endl;

        thr /= 6241.509125;

        // Fill position and angle vector
        x.push_back(posX);
        y.push_back(posY);
        z.push_back(posZ);
        dx.push_back(degX);
        dy.push_back(degY);
        dz.push_back(degZ);
        th.push_back(thr);
    }
    // Close the input efficiency file
    fclose(align_file);

    // Fill alignment graphs
    for (unsigned int n = 0; n < x.size(); n++) {
        g_posX->AddPoint(th.at(n), x.at(n));
        g_posY->AddPoint(th.at(n), y.at(n));
        g_posZ->AddPoint(th.at(n), z.at(n));
        g_degX->AddPoint(th.at(n), dx.at(n));
        g_degY->AddPoint(th.at(n), dy.at(n));
        g_degZ->AddPoint(th.at(n), dz.at(n));
    }

    // Read resolutions from the input CSV file and fill the graph
    std::cout << "Generating resolution graph" << std::endl;
    float res{}, res_err{};
    while (fgets(line, 100, resol_file)) {
        // Skip comment lines
        if (line[0] == '#')
            continue;

        // Read the next line
        sscanf(line, "%f,%f,%f", &thr, &res, &res_err);

        if (std::find(skip_runs.begin(), skip_runs.end(), thr) != skip_runs.end()) {
                std::cout << "Skipping threshold " << thr << std::endl;
                continue;
        }

        if (res > 100 || thr < 2000) continue;

        thr /= 6241.509125;

        if (debug) std::cout << "THR=" << thr << " : RES=(" << res << " +- " << res_err << ")" << std::endl;

        // Fill graph and set errors
        g_res->AddPoint(thr, res);
        g_res->SetPointError(g_res->GetN()-1, 0, res_err);
    }
    // Close the input efficiency file
    fclose(resol_file);

    // -----------------------------
    // ----------- CANVAS ----------
    // -----------------------------

    // Canvas
    auto canvas = new TCanvas("canvas", "canvas", 1600, 1200);
    canvas->SetLeftMargin(0.15);

    // -----------------------------
    // ---------- OPTIONS ----------
    // -----------------------------

    // Axes ranges
    auto thr_min = 0.0;
    auto thr_max = 8;
    auto eff_max = 1.05;
    auto eff_min = 0.0;

   // Requirements for operational window
    auto eff_req = 0.99;

    std::ostringstream oss;
    oss << std::noshowpoint << eff_req;
    auto eff_req_print = oss.str();

    // Efficiency color
    unsigned int main_color = kBlue-4;

    // -----------------------------
    //---------- MAIN ALG ----------
    // -----------------------------

    // Cluster size draw options
    g_clus->SetLineColor(main_color);
    g_clus->SetMarkerStyle(20);
    g_clus->SetMarkerSize(1);
    // g_clus->GetXaxis()->SetLimits(thr_min, thr_max);
    // g_clus->GetYaxis()->SetRangeUser(eff_min, eff_max);
    // g_clus->GetYaxis()->SetLabelColor(main_color);
    // g_clus->GetYaxis()->SetAxisColor(main_color);
    // g_clus->GetYaxis()->SetTitleColor(main_color);
    g_clus->SetLineColor(1);
    g_clus->SetDrawOption("AP");
    // Draw efficiency
    // g_clus->Draw("AP");

    // Efficiency fit function
    auto fit_form = "0.5*[0]*TMath::Erfc((x-[1])/(TMath::Sqrt(2)*[2])*(1-0.6*TMath::TanH([3]*(x-[1])/TMath::Sqrt(2)*[2])))";
    auto fit_func = new TF1("Efficiency_fit", fit_form, 0, 6);
    // Set initial parameter values and limits
    fit_func->SetParameters(1, 4, 1, 1, 0.5);
    fit_func->SetLineColor(main_color);

    // Efficiency draw options
    g_eff->SetLineColor(4);
    g_eff->SetMarkerColor(4);
    g_eff->SetMarkerStyle(20);
    g_eff->SetMarkerSize(1.5);
    g_eff->GetXaxis()->SetLimits(thr_min, thr_max);
    g_eff->GetYaxis()->SetRangeUser(eff_min, eff_max);
    // g_eff->GetYaxis()->SetLabelColor(main_color);
    // g_eff->GetYaxis()->SetAxisColor(main_color);
    // g_eff->GetYaxis()->SetTitleColor(main_color);
    // Perform fit

    // Draw efficiency
    g_eff->Draw("AP");

    if (fit_efficiency) {
        g_eff->Fit(fit_func, "NQR");
        fit_func->Draw("same");
    }

    // Draw efficiency requirement line
    auto eff_line = new TLine(thr_min, eff_req, thr_max, eff_req);
    eff_line->SetLineStyle(2);
    eff_line->SetLineColor(main_color);
    eff_line->Draw();

    // Add fit description
    auto fit_notes = new TPaveText(0.67, 0.55, 0.85, 0.70);
    fit_notes->SetTextSize(0.025);
    fit_notes->SetFillColor(0);
    fit_notes->ConvertNDCtoPad();
    fit_notes->SetTextAlign(11);
    fit_notes->AddText(TString("  #varepsilon_{max} = " + std::to_string(fit_func->GetParameter(0))));
    fit_notes->AddText(TString("   Vt_{50} = " + std::to_string(fit_func->GetParameter(1)) + " fC"));
    fit_notes->AddText(TString("#chi^{2}/ndf = " + std::to_string(fit_func->GetChisquare()/fit_func->GetNDF())));
    std::cout << "Vt50 = " << fit_func->GetParameter(1) << " +- " << fit_func->GetParError(1) << " fC" << std::endl;
    if (fit_efficiency)
        fit_notes->Draw("same");

    // Add legend
    auto legend = new TLegend(0.7, 0.70, 0.85, 0.80);
    legend->SetTextSize(0.03);
    legend->SetFillColorAlpha(0, 0.5);
    legend->SetBorderSize(0);
    legend->AddEntry(g_eff, "Eff", "pe");
    if (fit_efficiency)
        legend->AddEntry(fit_func, "Fit", "l");
    legend->AddEntry(eff_line, TString("Eff = " + eff_req_print), "l");
    legend->Draw("same");

    // Save output
    canvas->SaveAs(("reco/" + campaign + "/" + geometry + "/results/efficiency.pdf").c_str());
    std::cout << "Writing graphs to output file" << std::endl;
    g_eff->Write();
    g_clus->Write();
    g_res->Write();
    g_posX->Write();
    g_posY->Write();
    g_posZ->Write();
    g_degX->Write();
    g_degY->Write();
    g_degZ->Write();

    output_file->Close();

    return 0;
}
